Aplicativo para o Code Challenge da GoWithFlow

Guia do App

- O Aplicativo foi feito pensando no MVVM-Network;
- Nenhum framework de terceiros foi utilizado, não houve a necessidade;
- O aplicativo pode rodar em MacOS, iPadOS e iOS;
- O aplicativo em 2 arquivos de teste unitário:
	- Teste de requisição e conversão de JSON;
	- Teste do filtro utilizado na tela principal.
