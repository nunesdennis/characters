//
//  CharacterCollectionViewCell.swift
//  Characters
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharacterCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var imageView: CharacterImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var imageWidth: NSLayoutConstraint!
	
	func prepareCell(withCharacter character: CharacterViewModel) {
		nameLabel.text = character.name
		imageView.image = UIImage(named: "placeHolderCharacter")
		imageWidth.constant = character.imageWidth
		character.addImage(toCharacterImageView: imageView)
	}
}
