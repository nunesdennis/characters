//
//  UrlGuide.swift
//  Characters
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

enum UrlGuide {
	
	static let charactersUrl:URL = URL(string: "https://rickandmortyapi.com/api/character/")!
}
