//
//  MessageGuide.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

enum MessageGuide {
  
  //MARK: Error Messenger
  static let connectionErrorTitle: String = "Error"
  static let connectionErrorMessage: String = "Sorry, We couldn't get your characters, try again later."
  
  static let genericErrorTitle: String = "Error"
  static let genericErrorMessage: String = "Sorry, There was a problem with the app, try again later."

}
