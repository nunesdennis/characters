//
//  StyleGuide.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

enum StyleGuide {
	
	private static var isPhone: Bool {
		return UIDevice.current.userInterfaceIdiom == .phone
	}
	private static var screenWidth: CGFloat {
		return UIScreen.main.bounds.width
	}
	
	static var characterCellSize: CGSize {
		let width = isPhone ? (screenWidth/2.0)-6.0 : (screenWidth/3.0)-9.0
		return CGSize(width: width, height: width)
	}
}
