//
//  CharactersListViewModel.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharactersListViewModel {
	
	private weak var controller: UICollectionViewController?
  private var allCharacters: [CharacterDetailViewModel] = []
  private var filteredCharacters: [CharacterDetailViewModel]?
  private var isLoadingCharacters = false
	private var charactersPerPage:Int = 20
  private var characters: [CharacterDetailViewModel] {
    return filteredCharacters ?? allCharacters
  }
  
  private var isFiltered: Bool {
    return filteredCharacters != nil
  }
  
	private var numberOfPages: Int = 1
	
	private var currentPageNumber:Int {
		let charactersPerPage = Double(self.charactersPerPage)
		let allCharactersCount = Double(allCharacters.count)
		let pageNumber = Int(round(allCharactersCount/charactersPerPage))
		
		return pageNumber
	}
	
  private var nextPage:Int {
		return currentPageNumber + 1
  }
  
  var needsFirstLoad: Bool {
    return characters.count == 0
  }
  
  var numberOfSections: Int {
    return 1
  }
  
  var numberOfItemsInSection: Int {
    return characters.count
  }
  
	var characterCellSize: CGSize {
		return StyleGuide.characterCellSize
	}
	
  init(collectionViewController: UICollectionViewController) {
    self.controller = collectionViewController
  }
  
	func loadCharacters(fromIndexPath indexPath: IndexPath? = nil) {
    if let indexPath = indexPath, !shouldLoadMoreCharacters(indexPath: indexPath) {
      return
    }
    isLoadingCharacters = true
    let loader = CharacterLoader(resultHandler: updateUI(_:_:))
		loader.loadCharacters(fromPage: nextPage)
  }
  
	private func shouldLoadExtraPage(indexPath: IndexPath) -> Bool {
		let row = indexPath.row
		let visiblePage = (row/charactersPerPage) + 1
		
		return (currentPageNumber - visiblePage) == 0
	}
	
  private func shouldLoadMoreCharacters(indexPath: IndexPath) -> Bool {
    return !isLoadingCharacters && shouldLoadExtraPage(indexPath: indexPath)
  }
  
  private func updateUI(_ characterList: CharacterList?, _ errorType:  RequestErrorType = .noError) {
    guard errorType == .noError else {
			let errorName = String(reflecting: errorType)
			NSLog("[Error] There was a problem when trying to load the charactersList %@", errorName)
			return
		}
		guard let characterList = characterList else {
			NSLog("[Error] There was a problem when trying to load the charactersList, Data is nil")
			return
		}
		let characters = characterList.results
		let characterDetailViewModels = characters.compactMap({ CharacterDetailViewModel(character: $0) })

		allCharacters.append(contentsOf: characterDetailViewModels)
		numberOfPages = characterList.info.pages
		
		controller?.collectionView.reloadData()
    isLoadingCharacters = false
  }
  
  private func showErrorMessage(forType errorType: RequestErrorType) {
		guard let controller = controller else { return }
    ErrorMessenger.showErrorMessage(forType: errorType, onViewController: controller)
  }
  
	func filterCharactersList(thatContains text: String) {
		if text.isEmpty {
			filteredCharacters = nil
		} else {
			filteredCharacters = allCharacters.filter({ character in
				let nameLowerCased = character.name.lowercased()
				let textLowerCased = text.lowercased()
				return nameLowerCased.contains(textLowerCased)
			})
		}
		controller?.collectionView.reloadData()
  }
  
  func character(ofIndexPath indexPath: IndexPath) -> CharacterDetailViewModel {
    return characters[indexPath.row]
  }
}
