//
//  CharacterViewModel.swift
//  Characters
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharacterViewModel {
  
	let character: Character
	
	var name: String {
		return character.name
	}
	
	var imageURL: URL? {
		return URL(string: character.imageUrl)
	}
	
	var imageWidth: CGFloat {
		return StyleGuide.characterCellSize.width
	}
	
	init(character: Character) {
		self.character = character
	}
	
	func addImage(toCharacterImageView characterImageView: CharacterImageView) {
		guard let imageURL = imageURL else { return }
		characterImageView.loadImage(ofURL: imageURL)
	}
}
