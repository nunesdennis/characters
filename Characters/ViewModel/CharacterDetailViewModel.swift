//
//  CharacterDetailViewModel.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharacterDetailViewModel: CharacterViewModel {
	
	var gender: String {
		return "Gender: \(character.gender)"
	}
	
	var numberOfEpisodes: String {
		return "Number of episodes: \(character.episodesUrl.count)"
	}
	
	var locationName: String {
		return "Location Name: \(character.location.name)"
	}
	
	private weak var locationTypeLabel:UILabel!
	
	func addLocationType(toLabel label: UILabel) {
		label.text = "---"
		locationTypeLabel = label
		let loader = LocationLoader(resultHandler: updateLocationLabel(_:_:))
		
		guard let url = URL(string: character.location.url) else { return }
		loader.loadLocation(fromURL: url)
	}
	
	private func updateLocationLabel(_ locationDetail: LocationDetail?, _ errorType:  RequestErrorType = .noError) {
    guard errorType == .noError else {
			let errorName = String(reflecting: errorType)
			NSLog("[Error] There was a problem when trying to load the location detail %@", errorName)
			return
		}
		guard let locationDetail = locationDetail else {
			NSLog("[Error] There was a problem when trying to load the location detail, Data is nil")
			return
		}
		
		locationTypeLabel.text = "Location Type: \(locationDetail.type)"
  }
}
