//
//  CharactersListCollectionViewController.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

private let reuseIdentifier = "CharacterCell"

class CharactersListCollectionViewController: UICollectionViewController {
	
	private var viewModel: CharactersListViewModel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		viewModel = CharactersListViewModel(collectionViewController: self)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if viewModel.needsFirstLoad {
			viewModel.loadCharacters()
		}
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		collectionView.reloadData()
	}
	
	override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		
		let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CollectionViewSearchBar", for: indexPath)
		
		return headerView
	}
	
	// MARK: UICollectionViewDataSource
	
	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		return viewModel.numberOfSections
	}
	
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return viewModel.numberOfItemsInSection
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CharacterCollectionViewCell else { return UICollectionViewCell() }

		let characterCellViewModel = viewModel.character(ofIndexPath: indexPath)
    cell.prepareCell(withCharacter: characterCellViewModel)

    return cell
	}
	
	// MARK: UICollectionViewDelegate
	
	override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
			viewModel.loadCharacters(fromIndexPath: indexPath)
	}
	
	//MARK: Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let characterDetailViewController = segue.destination as? CharacterDetailViewController else { return }
		guard let indexPath = collectionView.indexPathsForSelectedItems?.first else { return }

		let characterDetailViewModel = viewModel.character(ofIndexPath: indexPath)
		characterDetailViewController.prepareController(withViewModel: characterDetailViewModel)
	}
}

// MARK: UICollectionViewDelegateFlowLayout
extension CharactersListCollectionViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return viewModel.characterCellSize
  }
}

// MARK: UISearchBarDelegate
extension CharactersListCollectionViewController: UISearchBarDelegate {
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		if let text = searchBar.text {
			viewModel.filterCharactersList(thatContains: text)
		}
	}
}
