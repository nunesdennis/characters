//
//  CharacterDetailViewController.swift
//  Characters
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController {
	
	private var viewModel: CharacterDetailViewModel!
	
	@IBOutlet weak var imageView: CharacterImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var genderLabel: UILabel!
	@IBOutlet weak var numberOfEpisodesLabel: UILabel!
	@IBOutlet weak var locationNameLabel: UILabel!
	@IBOutlet weak var locationTypeLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		nameLabel.text = viewModel.name
		genderLabel.text = viewModel.gender
		numberOfEpisodesLabel.text = viewModel.numberOfEpisodes
		locationNameLabel.text = viewModel.locationName
		viewModel.addLocationType(toLabel: locationTypeLabel)
		viewModel.addImage(toCharacterImageView: imageView)
	}
	
	func prepareController(withViewModel viewModel: CharacterDetailViewModel) {
		self.viewModel = viewModel
	}
}
