//
//  Location.swift
//  Characters
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct Location: Codable {
	
	let name: String
	let url: String
}
