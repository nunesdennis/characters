//
//  Character.swift
//  Characters
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct Character: Codable {
	
	let id: Int64
	let name: String
	let imageUrl: String
	let gender: String
	let episodesUrl: [String]
	let location: Location
	
	enum CodingKeys: String, CodingKey {
		case id
		case name
		case imageUrl = "image"
		case gender
		case episodesUrl = "episode"
		case location
	}
}
