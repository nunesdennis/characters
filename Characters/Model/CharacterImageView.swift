//
//  CharacterImageView.swift
//  Characters
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSURL, UIImage>()

class CharacterImageView: UIImageView {
	
	var sessionTask: URLSessionDataTask?
	
	func loadImage(ofURL url: URL) {
		sessionTask?.cancel()
		
		let nsURL = url as NSURL
		
		if let imageFromCache = imageCache.object(forKey: nsURL) {
			self.image = imageFromCache
			return
		}
		
		sessionTask = URLSession.shared.dataTask(with: url) { data, response, error in
			if error != nil {
				return
			}
			
			DispatchQueue.main.async {
				if let data = data, let image = UIImage(data: data) {
					imageCache.setObject(image, forKey: nsURL)
					UIView.transition(with: self,
														duration:0.5,
														options: .transitionCrossDissolve,
														animations: { self.image = image },
														completion: nil)
				}
			}
		}
		
		sessionTask?.resume()
	}
}
