//
//  LocationDetail.swift
//  Characters
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct LocationDetail: Codable {
	
	let id: Int64
	let name: String
	let type: String
}
