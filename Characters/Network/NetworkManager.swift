//
//  NetworkManager.swift
//  Characters
//
//  Created by Dennis Nunes on 05/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

enum RequestErrorType {
  case connectionError
  case parseError
  case genericError
  case noError
}

protocol NetworkResquester {
  func handleResult(data: Data?, errorType: RequestErrorType)
}

class NetworkManager {
  
  private let networkRequester:NetworkResquester!
  
  private var sharedSession: URLSession {
    return URLSession.shared
  }
  
  init(networkRequester: NetworkResquester) {
    self.networkRequester = networkRequester
  }
  
  func getRequest(withUrl url: URL) {
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    
    let task = sharedSession.dataTask(with: request) { data, response, error in
      DispatchQueue.main.async {
        self.requestHandler(data: data, response: response, error: error)
      }
    }
    
    task.resume()
  }
  
  func requestHandler(data: Data?, response: URLResponse?, error: Error?) {
		if let error = error as NSError?, error.code == -1009{
			networkRequester.handleResult(data: data, errorType: .connectionError)
			return
		}
		
    if let httpResponse = response as? HTTPURLResponse{
      switch httpResponse.statusCode {
      case 200:
        if let data = data {
          networkRequester.handleResult(data: data, errorType: .noError)
        }
      default:
        networkRequester.handleResult(data: nil, errorType: .genericError)
        break
      }
    }
  }
}
