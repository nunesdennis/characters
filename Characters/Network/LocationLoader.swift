//
//  LocationLoader.swift
//  Characters
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

class LocationLoader: NetworkResquester {

	typealias ResultHandler = (_ locationDetail: LocationDetail?,_ errorType: RequestErrorType) -> Void
	private let resultHandler: ResultHandler
	
	init(resultHandler: @escaping ResultHandler) {
		self.resultHandler = resultHandler
	}
	
	func loadLocation(fromURL url: URL) {
		let networkManager = NetworkManager(networkRequester: self)
		networkManager.getRequest(withUrl: url)
	}
	
	func handleResult(data: Data?, errorType: RequestErrorType) {
		guard errorType == .noError else {
			resultHandler(nil, errorType)
			
			return
		}
		
		guard let data = data else {
			resultHandler(nil, errorType)
			
			return
		}
		
		let decoder = JSONDecoder()
		
		let locationDetail = try? decoder.decode(LocationDetail.self, from: data)
		resultHandler(locationDetail, .noError)
	}
}
