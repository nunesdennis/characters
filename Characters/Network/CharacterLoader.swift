//
//  CharacterLoader.swift
//  Characters
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class CharacterLoader: NetworkResquester {

	typealias ResultHandler = (_ characterList: CharacterList?,_ errorType: RequestErrorType) -> Void
	private let resultHandler: ResultHandler
	
	init(resultHandler: @escaping ResultHandler) {
		self.resultHandler = resultHandler
	}
	
	func loadCharacters(fromPage page: Int?=0) {
		let networkManager = NetworkManager(networkRequester: self)
		
		var urlString = UrlGuide.charactersUrl.absoluteString
		if let pageNumber = page {
			urlString = UrlGuide.charactersUrl.absoluteString + "/?page=\(pageNumber)"
		}
		
		if let url = URL(string: urlString) {
			networkManager.getRequest(withUrl: url)
		}
	}
	
	func handleResult(data: Data?, errorType: RequestErrorType) {
		guard errorType == .noError else {
			resultHandler(nil, errorType)
			
			return
		}
		
		guard let data = data else {
			resultHandler(nil, errorType)
			
			return
		}
		
		let decoder = JSONDecoder()
		
		let characterList = try? decoder.decode(CharacterList.self, from: data)
		resultHandler(characterList, .noError)
	}
}
