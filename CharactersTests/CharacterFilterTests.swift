//
//  CharacterFilterTests.swift
//  CharactersTests
//
//  Created by Dennis Nunes on 11/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import XCTest
@testable import Characters

class CharacterFilterTests: XCTestCase {
	
	override func setUp() {}
	
	override func tearDown() {}
	
	func testFilter() {
		let expectation = self.expectation(description: "Network Request")
		
		let characterListViewModel = CharactersListViewModel(collectionViewController: UICollectionViewController())
		characterListViewModel.loadCharacters()
		
		DispatchQueue.global().async {
			while characterListViewModel.numberOfItemsInSection == 0 {}
			DispatchQueue.main.async {
				XCTAssert(characterListViewModel.numberOfItemsInSection == 20)
				characterListViewModel.filterCharactersList(thatContains: "Rick")
				XCTAssert(characterListViewModel.numberOfItemsInSection == 4)
				characterListViewModel.filterCharactersList(thatContains: "")
				XCTAssert(characterListViewModel.numberOfItemsInSection == 20)
				expectation.fulfill()
			}
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}
}
