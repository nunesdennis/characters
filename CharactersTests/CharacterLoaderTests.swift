//
//  CharacterLoaderTests.swift
//  CharactersTests
//
//  Created by Dennis Nunes on 06/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import XCTest
@testable import Characters

class CharacterLoaderTests: XCTestCase, NetworkResquester {
	
	var expectation: XCTestExpectation!
	
	override func setUp() {}
	
	override func tearDown() {}
	
	func handleResult(data: Data?, errorType: RequestErrorType) {
		guard errorType == .noError else {
			XCTFail("Error \(errorType)")
			return
		}
		guard let data = data else {
			XCTFail("Data came nil from the Characters request.")
			return
		}
		
		let decoder = JSONDecoder()
		
		do {
			_ = try decoder.decode(CharacterList.self, from: data)
		} catch let error {
			let dictionaryFromJSON = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
			print(dictionaryFromJSON ?? "printing dictionary from JSON returned empty")
			
			XCTFail(error.localizedDescription)
		}
		
		expectation.fulfill()
	}
	
	func testLoader() {
		expectation = self.expectation(description: "Network Request")
		
		let networkManager = NetworkManager(networkRequester: self)
		
		networkManager.getRequest(withUrl: UrlGuide.charactersUrl)
		waitForExpectations(timeout: 5, handler: nil)
	}
}
